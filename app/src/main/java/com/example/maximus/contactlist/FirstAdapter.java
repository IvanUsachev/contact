package com.example.maximus.contactlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class FirstAdapter extends BaseAdapter {
    private Context context;
    private List<Contact> contacts;

    public FirstAdapter(Context context, List<Contact> contacts) {
        this.context = context;
        this.contacts = contacts;
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public Contact getItem(int position) {
        return contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return contacts.get(position).hashCode();
    }

    private static class ViewHolder {
        TextView name;
        TextView email;
        ImageView photo;

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder holder;
        if (rowView == null) {
            rowView = LayoutInflater.from(context)
                    .inflate(R.layout.activity_contact, parent, false);
            holder = new
                    ViewHolder();
            holder.name = (TextView) rowView.findViewById(R.id.name);
            holder.email = (TextView) rowView.findViewById(R.id.email);
            holder.photo = (ImageView) rowView.findViewById(R.id.photo);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.name.setText(getItem(position).getName());
        holder.email.setText(getItem(position).getEmail());
        holder.photo.setImageResource(getItem(position).getPhoto());
        return rowView;
    }






}

