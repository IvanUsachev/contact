package com.example.maximus.contactlist;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    FirstAdapter firstAdapter;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView contactList = (ListView) findViewById(R.id.contact_list);
        contactList.setAdapter(new FirstAdapter(this, Generator.generate()));
        firstAdapter = new FirstAdapter(this,Generator.generate());

        button = (Button) findViewById(R.id.add_contact);
        button.setOnClickListener(this);

        contactList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, InfoContactActivity.class);
                intent.putExtra("name", firstAdapter.getItem(position).getName());
                intent.putExtra("address", firstAdapter.getItem(position).getAddress());
                intent.putExtra("email", firstAdapter.getItem(position).getEmail());
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(MainActivity.this,InfoContactActivity.class);
        startActivity(intent);
    }
}
