package com.example.maximus.contactlist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class InfoContactActivity extends AppCompatActivity {

    TextView name;
    TextView email;
    TextView address;
    ImageView photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_contact);

        String nameValue;
        String addressValue;
        String emailValue;

        nameValue = getIntent().getStringExtra("name");
        addressValue = getIntent().getStringExtra("address");
        emailValue = getIntent().getExtras().getString("email");

        name = (TextView) findViewById(R.id.name);
        email = (TextView) findViewById(R.id.email);
        address = (TextView) findViewById(R.id.address);
        photo = (ImageView) findViewById(R.id.photo);

        name.setText("Name: " + nameValue);
        email.setText("E-mail: " + addressValue);
        address.setText("Address: " + emailValue);
    }
}

